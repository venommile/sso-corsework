import {createApp} from 'vue'
import App from './App.vue'
import {createRouter, createWebHistory} from "vue-router";
import LoginService from "@/services/login-service";

import Home from './views/home.vue'
import Login from './views/login.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});


router.beforeEach((to, from, next) => {

    // если путь равен /code, то пытаемся достать параметр code из запроса, запросить токены, и после их получения
    // сделать переход на домашнюю страницу
    if (to.path === '/code' && to.query.code != null) {
        LoginService.getTokens(to.query.code).then(() => {
            next({name: 'home'});
        });
    } else {
        next()
    }
})

createApp(App)
    .use(router)
    .mount('#app')