package ru.galtsev.courseworksso

import org.junit.jupiter.api.Test
import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter
import ru.galtsev.courseworksso.user.UserModule

class ArchTest {
    @Test
    fun verifyModules() {
        val modules = ApplicationModules.of(CourseWorkSsoApplication::class.java)

        modules.verify()
    }


    @Test
    fun createModuleDocumentation() {
        val modules: ApplicationModules = ApplicationModules.of(CourseWorkSsoApplication::class.java)
        Documenter(modules)
            .writeDocumentation()
            .writeIndividualModulesAsPlantUml()
    }

    @Test
    fun createUserModuleDocumentation() {
        val modules: ApplicationModules = ApplicationModules.of(UserModule::class.java)
        Documenter(modules)
            .writeDocumentation()
            .writeIndividualModulesAsPlantUml()
    }
}
