package ru.galtsev.courseworksso.user.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.galtsev.courseworksso.user.entity.UserEntity
import java.util.*


interface UserDaoRepository : JpaRepository<UserEntity?, UUID?> {
    fun findByEmail(email: String?): Optional<UserEntity?>
    fun findBySecretWord(secretWord: String): Optional<UserEntity?>
}
