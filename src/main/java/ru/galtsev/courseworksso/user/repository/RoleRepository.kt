package ru.galtsev.courseworksso.user.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.galtsev.courseworksso.user.entity.RoleEntity

interface RoleRepository : JpaRepository<RoleEntity?, Long?> {


    @Query("select r from RoleEntity r where r.code = 'USER_SSO'")
    fun defaultRole(): RoleEntity?
}
