package ru.galtsev.courseworksso.user.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.galtsev.courseworksso.user.entity.AuthorityEntity

interface AuthorityRepository : JpaRepository<AuthorityEntity?, String?>