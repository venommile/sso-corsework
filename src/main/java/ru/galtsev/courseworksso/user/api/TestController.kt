package ru.galtsev.courseworksso.user.api

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController {
    @GetMapping("/test")
    fun test(): Authentication {
        val authentication = SecurityContextHolder.getContext().authentication
        return authentication
    }


}
