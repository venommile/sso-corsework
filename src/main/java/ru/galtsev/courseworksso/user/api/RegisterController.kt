package ru.galtsev.courseworksso.user.api

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.galtsev.courseworksso.dto.RegisterUser
import ru.galtsev.courseworksso.user.entity.UserEntity
import ru.galtsev.courseworksso.user.service.DBUserService


@RestController
class RegisterController(
    private val dbUserService: DBUserService
) {


    @PostMapping("/users")
    fun createUser(@RequestBody registerUser: RegisterUser): UserEntity {
        return dbUserService.save(registerUser)
    }

}
