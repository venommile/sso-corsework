package ru.galtsev.courseworksso.user.service

import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Service
import ru.galtsev.courseworksso.user.enums.AuthProvider

@Service
class CustomOAuth2UserService(
    private val userService: UserService
) : DefaultOAuth2UserService() {


    override fun loadUser(userRequest: OAuth2UserRequest): OAuth2User {
        val oAuth2User = super.loadUser(userRequest) // Загружаем пользователя, как это было до
        val clientRegId =
            userRequest.clientRegistration.registrationId // Получаем наименование провайдера (google, github и т.д.)
        val provider = AuthProvider.findByName(clientRegId)

        // Для удобства создадим enum AuthProvider и по наименованию провайдера получим значение
        return userService.saveAndMap(
            oAuth2User,
            provider
        ) // Создадим дополнительный сервис UserService, в котором опишем сохранение пользователя при его отсутствии в БД, а также маппинг на AuthorizedUser
    }
}
