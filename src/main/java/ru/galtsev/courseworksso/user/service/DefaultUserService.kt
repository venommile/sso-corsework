package ru.galtsev.courseworksso.user.service

import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Service
import ru.galtsev.courseworksso.dto.AuthorizedUser
import ru.galtsev.courseworksso.user.entity.UserEntity
import ru.galtsev.courseworksso.user.enums.AuthErrorCode
import ru.galtsev.courseworksso.user.enums.AuthProvider
import ru.galtsev.courseworksso.user.exception.AuthException
import ru.galtsev.courseworksso.user.mapper.AuthorizedUserMapper

@Service
class DefaultUserService(
    private val dbUserService: DBUserService,
    private val userMapper: AuthorizedUserMapper
) : UserService {


    /**
     * Создание или обновление пользователя
     */
    override fun save(userDto: OAuth2User?, provider: AuthProvider?): UserEntity {
        return when (provider) {
            AuthProvider.GITHUB -> this.saveUserFromGithab(userDto)
            AuthProvider.YANDEX -> this.saveUserFromYandex(userDto)
            null -> TODO()
        }
    }


    private fun saveUserFromYandex(userDto: OAuth2User?): UserEntity {
        var login = userDto!!.getAttribute<String>("default_email")
        if (login == null) {
            login = userDto.getAttribute("login")
            if (login == null) {
                throw AuthException(AuthErrorCode.EMAIL_IS_EMPTY)
            }
        }

        val userEntityOptional = dbUserService.findByEmail(login)
        val user: UserEntity // пытаемся найти пользователя в нашем хранилище по email
        if (userEntityOptional.isEmpty) {                                           // если пользователя не существует у нас, то создаём новую сущность UserEntity
            user = UserEntity(email = login)
            user.email = login
            user.active = true // пока пусть все созданные пользователи будут активными
        } else {
            user = userEntityOptional.get()
        }
        val firstName = userDto.getAttribute<String>("first_name")
        val lastName = userDto.getAttribute<String>("last_name")
        if (firstName != null) {             // получаем firstName, lastName и middleName
            user.firstName = firstName
        }
        if (lastName != null) {
            user.secondName = lastName
        }

        return dbUserService.save(user)
    }

    /**
     * Создание или обновление пользователя с последующим маппингом в сущность AuthorizedUser
     */
    override fun saveAndMap(userDto: OAuth2User?, provider: AuthProvider?): AuthorizedUser {
        val entity = this.save(userDto, provider)
        return userMapper.map(entity)
    }


    /**
     * Метод описывающий создание/обновление UserEntity на основе OAuth2User полученного из провайдера Github
     */
    private fun saveUserFromGithab(userDto: OAuth2User?): UserEntity {
        var login = userDto!!.getAttribute<String>("email") // пытаемся получить атрибут email
        if (login == null) {
            login = userDto.getAttribute("login")
            if (login == null) {
                throw AuthException(AuthErrorCode.EMAIL_IS_EMPTY)
            }
        }
        val userOpt = dbUserService.findByEmail(login)
        val user: UserEntity // пытаемся найти пользователя в нашем хранилище по email
        if (userOpt!!.isEmpty) {                                           // если пользователя не существует у нас, то создаём новую сущность UserEntity
            user = UserEntity(email = login)
            user.email = login
            user.active = true // пока пусть все созданные пользователи будут активными
        } else {
            user = userOpt.get()
        }

        if (userDto.getAttribute<Any?>("name") != null) {             // получаем firstName, lastName и middleName
            val splitted =
                (userDto.getAttribute<Any>("name") as String).split(regex = " ".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()
            user.firstName = splitted[0]
            if (splitted.size > 1) {
                user.secondName = splitted[1]
            }
            if (splitted.size > 2) {
                user.middleName = splitted[2]
            }
        } else {                                                      // иначе устанавливаем в эти поля значение email
            user.firstName =
                userDto.getAttribute("login") // конечно в реальных проектах так делать не надо, здесь это сделано для упрощения логики
            user.secondName = userDto.getAttribute("login")
        }

        if (userDto.getAttribute<Any?>("avatar_url") != null) {       // если есть аватар, то устанавливаем значение в поле avatarUrl
            user.avatarUrl = userDto.getAttribute("avatar_url")
        }
        return dbUserService.save(user) // сохраняем сущность UserEntity
    }
}
