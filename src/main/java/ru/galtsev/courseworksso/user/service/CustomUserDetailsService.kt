package ru.galtsev.courseworksso.user.service

import lombok.RequiredArgsConstructor
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.galtsev.courseworksso.user.mapper.AuthorizedUserMapper


@Transactional
@RequiredArgsConstructor
@Service
class CustomUserDetailsService(
    private val dbUserService: DBUserService,
    private val userMapper: AuthorizedUserMapper
) : UserDetailsService {


    override fun loadUserByUsername(username: String): UserDetails {
        val entity = dbUserService.findByEmail(username)
        if (entity.isEmpty) {
            throw UsernameNotFoundException("User with username = $username not found")
        }
        return userMapper.map(entity.get())
    }
}
