package ru.galtsev.courseworksso.user.service

import org.springframework.security.oauth2.core.user.OAuth2User
import ru.galtsev.courseworksso.dto.AuthorizedUser
import ru.galtsev.courseworksso.user.entity.UserEntity
import ru.galtsev.courseworksso.user.enums.AuthProvider

interface UserService {
    fun save(userDto: OAuth2User?, provider: AuthProvider?): UserEntity

    fun saveAndMap(userDto: OAuth2User?, provider: AuthProvider?): AuthorizedUser
}
