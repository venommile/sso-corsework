package ru.galtsev.courseworksso.user.service

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.server.ServletServerHttpResponse
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2TokenIntrospectionAuthenticationToken
import org.springframework.stereotype.Service
import ru.galtsev.courseworksso.dto.AuthorizedUser
import ru.galtsev.courseworksso.dto.IntrospectionPrincipal
import ru.galtsev.courseworksso.dto.TokenInfoDto
import java.io.IOException


@Service
class IntrospectionService(
    private val oAuth2AuthorizationService: OAuth2AuthorizationService,
    private val mappingJackson2HttpMessageConverter: MappingJackson2HttpMessageConverter
) {

    @Throws(IOException::class)
    fun introspectionResponse(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authentication: Authentication
    ) {
        val introspectionAuthenticationToken = authentication as OAuth2TokenIntrospectionAuthenticationToken

        val tokenInfoDtoBuilder = TokenInfoDto(active = false)
        if (introspectionAuthenticationToken.tokenClaims.isActive) {                                                 // если токен активен, то заполняем все параметры информации о токене и далее пытаемся получить информацию о пользователе
            val claims = introspectionAuthenticationToken.tokenClaims
            tokenInfoDtoBuilder.active = true
            tokenInfoDtoBuilder.sub = claims.subject
            tokenInfoDtoBuilder.aud = claims.audience
            tokenInfoDtoBuilder.nbf = claims.notBefore
            tokenInfoDtoBuilder.scopes = claims.scopes
            tokenInfoDtoBuilder.iss = claims.issuer
            tokenInfoDtoBuilder.exp = claims.expiresAt
            tokenInfoDtoBuilder.iat = claims.issuedAt
            tokenInfoDtoBuilder.jti = claims.id
            tokenInfoDtoBuilder.clientId = claims.clientId
            tokenInfoDtoBuilder.tokenType = claims.tokenType


            val token = introspectionAuthenticationToken.token // получаем значение токена, который проверяется
            val tokenAuth = oAuth2AuthorizationService.findByToken(
                token,
                OAuth2TokenType.ACCESS_TOKEN
            ) // предполагая что это ACCESS TOKEN, пытаемся получить объект OAuth2Authorization из OAuth2AuthorizationService
            if (tokenAuth != null) {
                val attributeAuth =
                    tokenAuth.getAttribute<Authentication>(principalAttributeKey) // Если найден этот объект OAuth2Authorization, то получаем из него объект Authentication следующим образом
                if (attributeAuth != null) {
                    val authorizedUser = attributeAuth.principal
                    if (authorizedUser is AuthorizedUser) {                             // Если полученный объект Authentication не пуст, то проверяем является ли его principal экземпляром класса AuthorizedUser
                        tokenInfoDtoBuilder.principal =
                            IntrospectionPrincipal.build(authorizedUser) // Создаём IntrospectionPrincipal на его основе
                    } else {                                                                                                 // Иначе выбрасываем исключение, что другие типы principal мы не поддерживаем
                        throw RuntimeException("Principal class = ${authorizedUser.javaClass.simpleName} is not supported")
                    }
                }
            }

        }

        val httpResponse = ServletServerHttpResponse(response)
        mappingJackson2HttpMessageConverter.write(
            tokenInfoDtoBuilder,
            null,
            httpResponse
        ) // Превращаем наш TokenInfoDto в json строку и отправляем её через ServletServerHttpResponse

    }

    companion object {
        private const val principalAttributeKey = "java.security.Principal"
    }
}