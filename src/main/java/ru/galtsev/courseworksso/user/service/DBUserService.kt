package ru.galtsev.courseworksso.user.service

import jakarta.annotation.PostConstruct
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import ru.galtsev.courseworksso.dto.AuthorizedUser
import ru.galtsev.courseworksso.dto.RegisterUser
import ru.galtsev.courseworksso.user.entity.UserEntity
import ru.galtsev.courseworksso.user.mapper.AuthorizedUserMapper
import ru.galtsev.courseworksso.user.repository.UserDaoRepository
import java.time.LocalDate
import java.util.*


@Service
class DBUserService(
    private val userRepository: UserDaoRepository,
    private val passwordEncoder: PasswordEncoder,
    private val mapper: AuthorizedUserMapper
) {


    fun save(user: UserEntity): UserEntity = userRepository.save(user)

    fun save(user: RegisterUser): UserEntity =
        save(UserEntity(email = user.userName, passwordHash = passwordEncoder.encode(user.password)))

    fun get(id: UUID): UserEntity = userRepository.findById(id).orElse(null)!!

    fun findByEmail(email: String?): Optional<UserEntity?> = userRepository.findByEmail(email)

    @PostConstruct
    fun afterPropertiesSet() {
        val user = UserEntity(email = "admin@example.com")
        user.passwordHash = "{noop}admin@example.com"
        user.active = true
        user.firstName = "Admin"
        user.secondName = "Admin"
        user.birthday = LocalDate.of(1998, 7, 14)
        userRepository.save(
            user
        )
    }

    fun findUserBySecretAndChange(secret: String): Optional<UserEntity?> {
        val res = userRepository.findBySecretWord(secret)

        if (res.isPresent) {
            val user = res.get()
            user.secretWord = null
            userRepository.save(user)
        }
        return res
    }

    fun findSecurityUserBySecret(secret: String): AuthorizedUser? {
        val userOpt = findUserBySecretAndChange(secret)

        if (userOpt.isPresent) {
            val user = mapper.map(userOpt.get())
            return user
        }
        return null
    }

    fun setUserSecretWord(userName: String, secret: String) {
        val userOpt = findByEmail(userName)

        val user = userOpt.orElseThrow()!!
        user.secretWord = secret

        save(user)
    }
}

