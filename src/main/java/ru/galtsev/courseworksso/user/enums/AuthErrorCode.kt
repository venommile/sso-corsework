package ru.galtsev.courseworksso.user.enums

enum class AuthErrorCode {
    EMAIL_IS_EMPTY,
    BAD_CREDENTIALS,
    AUTH_ERROR
}
