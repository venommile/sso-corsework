package ru.galtsev.courseworksso.user.enums

import java.util.stream.Stream

enum class AuthProvider(val providerName: String) {
    GITHUB("github"),
    YANDEX("yandex");

    companion object {
        fun findByName(providerName: String): AuthProvider? {
            return Stream.of(*entries.toTypedArray())
                .filter { item: AuthProvider? -> item!!.providerName == providerName }
                .findFirst()
                .orElse(null)
        }
    }
}
