package ru.galtsev.courseworksso.user.entity

import jakarta.persistence.*
import org.hibernate.annotations.UuidGenerator
import java.util.*

@Entity
@Table
class RoleEntity {
    @Id
    @Column(name = "role_id", nullable = false)
    @UuidGenerator
    private var id: UUID? = null

    @Column(name = "role_code", nullable = false)
    private var code: String? = "test-role-code"

    @Column(name = "role_description", nullable = false)
    private var description: String? = "test-role-desc"

    @Column(name = "active")
    private var active: Boolean = true

    @ManyToMany
    var authorities: List<AuthorityEntity>? = null

    @ManyToMany
    var users: List<UserEntity>? = null
}
