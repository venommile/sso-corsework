package ru.galtsev.courseworksso.user.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import org.hibernate.annotations.UuidGenerator
import java.util.*

@Entity
class AuthorityEntity {
    @Id
    @Column(name = "authority_id", nullable = false)
    @UuidGenerator
    var id: UUID? = null

    @Column(name = "authority_code", nullable = false)
    var code: String = "test-authority-code"

    @Column(name = "authority_description", nullable = false)
    var description: String? = null

    @Column(name = "system_code", nullable = false)
    var systemCode: String = "test-system-code"

    @Column(name = "active")
    var active: Boolean? = null

}
