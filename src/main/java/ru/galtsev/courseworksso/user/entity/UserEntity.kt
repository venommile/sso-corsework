package ru.galtsev.courseworksso.user.entity

import jakarta.persistence.*
import org.hibernate.annotations.UuidGenerator
import java.time.LocalDate
import java.util.*

@Entity
@Table
class UserEntity(
    @Id
    @UuidGenerator
    var id: UUID? = null,
    @Column
    var email: String,
    @Column
    var passwordHash: String? = null,
    @Column
    var firstName: String? = null,
    @Column
    var secondName: String? = null,
    @Column
    var middleName: String? = null,
    @Column
    var birthday: LocalDate? = null,
    @Column
    var avatarUrl: String? = null,
    @Column
    var active: Boolean? = null,

    @ManyToMany
    var role: List<RoleEntity>? = null,
    @Column
    var secretWord: String? = null
)
