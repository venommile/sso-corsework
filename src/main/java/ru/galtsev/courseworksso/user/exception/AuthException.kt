package ru.galtsev.courseworksso.user.exception

import org.springframework.security.core.AuthenticationException
import ru.galtsev.courseworksso.user.enums.AuthErrorCode

class AuthException : AuthenticationException {
    var errorCode: AuthErrorCode

    constructor(errorCode: AuthErrorCode, msg: String?, cause: Throwable?) : super(msg, cause) {
        this.errorCode = errorCode
    }

    constructor(msg: String?, errorCode: AuthErrorCode) : super(msg) {
        this.errorCode = errorCode
    }

    constructor(errorCode: AuthErrorCode) : super(null) {
        this.errorCode = errorCode
    }
}
