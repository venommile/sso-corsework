package ru.galtsev.courseworksso.user.mapper

import org.springframework.stereotype.Component
import ru.galtsev.courseworksso.dto.AuthorizedUser
import ru.galtsev.courseworksso.user.entity.UserEntity


@Component
class AuthorizedUserMapper {
    fun map(entity: UserEntity): AuthorizedUser {
        return AuthorizedUser(
            id = entity.id,
            email = entity.email,
            password = entity.passwordHash,
            firstName = entity.firstName,
            middleName = entity.middleName,
            birthday = entity.birthday,
            avatarUrl = entity.avatarUrl
        )

    }
}
