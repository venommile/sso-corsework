package ru.galtsev.courseworksso.config

import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler

class SocialConfigurer(
    var oAuth2UserService: OAuth2UserService<OAuth2UserRequest, OAuth2User>? = null,
    var failureHandler: AuthenticationFailureHandler? = null,
    var successHandler: AuthenticationSuccessHandler = SavedRequestAwareAuthenticationSuccessHandler()
) : AbstractHttpConfigurer<SocialConfigurer, HttpSecurity>() {


    @Throws(Exception::class)
    override fun init(http: HttpSecurity) {
        http.oauth2Login { oauth2Login: OAuth2LoginConfigurer<HttpSecurity?> ->
            oauth2Login.userInfoEndpoint().userService(this.oAuth2UserService)
            oauth2Login.successHandler(this.successHandler)
            oauth2Login.failureHandler(this.failureHandler)
        }
    }
}
