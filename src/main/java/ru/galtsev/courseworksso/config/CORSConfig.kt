package ru.galtsev.courseworksso.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
class CORSConfig {
    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowCredentials = true
        configuration.allowedOrigins =
            mutableListOf("http://localhost:8080/", "http://192.168.31.83:8080/")
        configuration.allowedMethods =
            mutableListOf("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH")
        configuration.maxAge = 3600L
        configuration.allowedHeaders = listOf("*")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}
