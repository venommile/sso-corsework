package ru.galtsev.courseworksso.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfigurationSource
import ru.galtsev.courseworksso.config.filter.JwtAuthenticationFilter
import ru.galtsev.courseworksso.user.service.CustomOAuth2UserService

@EnableWebSecurity(debug = true)
@Configuration
class SecurityConfig(
    private val customOAuth2UserService: CustomOAuth2UserService,
    private val userDetailService: UserDetailsService,
    private val passwordEncoder: PasswordEncoder,
    private val corsConfigurationSource: CorsConfigurationSource,
    private val secretWordAuthFilter: JwtAuthenticationFilter

) {


    @Bean
    @Throws(Exception::class)
    fun defaultSecurityFilterChain(http: HttpSecurity): SecurityFilterChain {
        val socialConfigurer = SocialConfigurer()
        socialConfigurer.oAuth2UserService = customOAuth2UserService


        http.apply(socialConfigurer)
        http.cors { it.configurationSource(corsConfigurationSource) }

        http.csrf { it.disable() }

        http.authenticationProvider(authenticationProvider())
        http.authorizeHttpRequests {
            it.requestMatchers("/qr/secret").permitAll()
            it.requestMatchers("/qr/auth/**").permitAll()
            it.requestMatchers("/users").permitAll()
            it.anyRequest().authenticated()
        }
        http.addFilterBefore(secretWordAuthFilter, UsernamePasswordAuthenticationFilter::class.java)
        return http.formLogin(Customizer.withDefaults()).build()
    }


    @Bean
    fun authenticationProvider(): AuthenticationProvider {

        val daoAuthenticationProvider = DaoAuthenticationProvider()
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder)
        daoAuthenticationProvider.setUserDetailsService(userDetailService)
        return daoAuthenticationProvider
    }
}
