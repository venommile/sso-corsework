package ru.galtsev.courseworksso.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "spring.security.oauth2.authorizationserver")
class AuthorizationServerProperties(
    var issuerUrl: String? = null,
    var introspectionEndpoint: String? = null
)
