package ru.galtsev.courseworksso.config.filter

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import ru.galtsev.courseworksso.user.service.DBUserService


@Component
class JwtAuthenticationFilter(
    private val userDetailsService: DBUserService,
) : OncePerRequestFilter() {


    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val secret = request.getHeader("secret")

        if (secret != null && secret.isNotBlank()) {
            val user = userDetailsService.findSecurityUserBySecret(secret)
            if (user != null) {
                val authToken = UsernamePasswordAuthenticationToken(
                    user,
                    null,
                    user.authorities
                )
                authToken.details = WebAuthenticationDetailsSource().buildDetails(request)
                getSecurityContext().setAuthentication(authToken)
            }
        }
        filterChain.doFilter(request, response)
    }

    private fun getSecurityContext(): SecurityContext {
        return SecurityContextHolder.getContext()
    }


}
