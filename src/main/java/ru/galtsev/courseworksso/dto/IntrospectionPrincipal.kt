package ru.galtsev.courseworksso.dto

import org.springframework.security.core.GrantedAuthority
import java.time.LocalDate
import java.util.*
import java.util.stream.Collectors

class IntrospectionPrincipal(
    var id: UUID? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var middleName: String? = null,
    var birthday: LocalDate? = null,
    var avatarUrl: String? = null,
    var username: String? = null,
    var email: String? = null,
    var authorities: List<String>? = null

) {

    companion object {
        fun build(authorizedUser: AuthorizedUser?): IntrospectionPrincipal? {
            if (authorizedUser == null) return null

            // создаём список строк из authorities в AuthorizedUser
            var authorities: List<String>? = emptyList()
            if (authorizedUser.authorities != null) {
                authorities = authorizedUser.authorities
                    .stream()
                    .map { obj: GrantedAuthority -> obj.authority }
                    .collect(Collectors.toList())
            }

            val principal = IntrospectionPrincipal()
            principal.id = authorizedUser.id
            principal.firstName = authorizedUser.firstName
            principal.lastName = authorizedUser.lastName
            principal.middleName = authorizedUser.middleName
            principal.birthday = authorizedUser.birthday
            principal.avatarUrl = authorizedUser.avatarUrl
            principal.username = authorizedUser.username
            principal.email = authorizedUser.email
            principal.authorities = authorities
            return principal
        }
    }
}
