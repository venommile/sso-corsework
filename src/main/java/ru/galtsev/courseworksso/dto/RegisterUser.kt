package ru.galtsev.courseworksso.dto

data class RegisterUser(
    val userName: String,
    val password: String
)
