package ru.galtsev.courseworksso.dto

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.oauth2.core.user.OAuth2User
import java.time.LocalDate
import java.util.*

class AuthorizedUser(
    var id: UUID? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var middleName: String? = null,
    var birthday: LocalDate? = null,
    var avatarUrl: String? = null,
    var email: String,
    username: String? = null,
    password: String? = null,
    enabled: Boolean = true,
    accountNonExpired: Boolean = true,
    credentialsNonExpired: Boolean = true,
    accountNonLocked: Boolean = true,
    authorities: Collection<GrantedAuthority?> = listOf()

) : User(
    email.ifBlank {
        username
    }, password?.ifBlank {
        "{noop}"
    } ?: "{noop}", enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities
),
    OAuth2User {
    override fun getName(): String {
        return email
    }

    override fun getAttributes(): MutableMap<String, Any> {
        return mutableMapOf()
    }


}
