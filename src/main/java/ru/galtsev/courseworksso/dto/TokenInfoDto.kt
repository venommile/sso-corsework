package ru.galtsev.courseworksso.dto

import java.net.URL
import java.time.Instant

class TokenInfoDto(
    var active: Boolean? = null,
    var sub: String? = null,
    var aud: List<String>? = null,
    var nbf: Instant? = null,
    var scopes: List<String>? = null,
    var iss: URL? = null,
    var exp: Instant? = null,
    var iat: Instant? = null,
    var jti: String? = null,
    var clientId: String? = null,
    var tokenType: String? = null,
    var principal: IntrospectionPrincipal? = null
)