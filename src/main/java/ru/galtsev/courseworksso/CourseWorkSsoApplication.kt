package ru.galtsev.courseworksso

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession

@SpringBootApplication
@EnableSpringHttpSession
class CourseWorkSsoApplication

fun main(args: Array<String>) {
    SpringApplication.run(CourseWorkSsoApplication::class.java, *args)
}
