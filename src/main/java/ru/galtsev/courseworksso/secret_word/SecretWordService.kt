package ru.galtsev.courseworksso.secret_word

import org.springframework.stereotype.Service
import ru.galtsev.courseworksso.user.service.DBUserService


@Service
class SecretWordService(
    private val userService: DBUserService
) {


    fun setUserSecretWord(userName: String, secret: String) {
        userService.setUserSecretWord(userName, secret)
    }


}