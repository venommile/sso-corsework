package ru.galtsev.courseworksso.secret_word.api

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.galtsev.courseworksso.secret_word.SecretWordService


@RestController
@RequestMapping("/secret")
class SecretWordController(
    private val secretWordService: SecretWordService
) {




    @GetMapping("/set-secret/{secret}")
    fun authBySecretWord(@PathVariable("secret") qrValue: String) {
        val authentication = SecurityContextHolder.getContext().authentication

        if (authentication.name != null) {
            secretWordService.setUserSecretWord(authentication.name, qrValue)
        }
    }


}